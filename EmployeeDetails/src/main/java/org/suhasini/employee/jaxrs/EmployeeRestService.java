package org.suhasini.employee.jaxrs;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.suhasini.employee.bean.Employee;
@Path("/countries")
public class EmployeeRestService {
	
    @GET
    @Produces(MediaType.APPLICATION_JSON)
	public List<Employee> getEmployees()
	{
		List<Employee> listOfEmployees = new ArrayList<Employee>();
		listOfEmployees=createEmployeeList();
		return listOfEmployees;
	}

	@GET
    @Path("{id: \\d+}")
    @Produces(MediaType.APPLICATION_JSON)
	public Employee getEmployeeById(@PathParam("id") int Employeeid)
	{
		List<Employee> listOfEmployees = new ArrayList<Employee>();
		listOfEmployees=createEmployeeList();

		for (Employee employee: listOfEmployees) {
			if(employee.getemployeeId()==Employeeid)
				return employee;
		}
		
		return null;
	}

// Utiliy method to create employee list.
	public List<Employee> createEmployeeList()
	{
		Employee e1=new Employee("ab", 13, "dc@hcl.com" 569874, getDate() );
		Employee e=new Country("bc" , 14, "ec@hcl.com" 98533, getDate() );

		List<Employee> listOfEmployees = new ArrayList<Employee>();
		listOfEmployees.add(e1);
		listOfEmployees.add(e2);
	
		return listOfEmployees
	}
}
