package org.suhasini.employee.bean;

public class Employee{
	
String Name;
    int EmployeeId;
    String EmailId;
	Long MobileNumber;
	Date DOJ;
	public Employee(String name, int employeeId, String emailId,
			Long mobileNumber, Date dOJ) {
		super();
		Name = name;
		EmployeeId = employeeId;
		EmailId = emailId;
		MobileNumber = mobileNumber;
		DOJ = dOJ;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public int getEmployeeId() {
		return EmployeeId;
	}
	public void setEmployeeId(int employeeId) {
		EmployeeId = employeeId;
	}
	public String getEmailId() {
		return EmailId;
	}
	public void setEmailId(String emailId) {
		EmailId = emailId;
	}
	public Long getMobileNumber() {
		return MobileNumber;
	}
	public void setMobileNumber(Long mobileNumber) {
		MobileNumber = mobileNumber;
	}
	public Date getDOJ() {
		return DOJ;
	}
	public void setDOJ(Date dOJ) {
		DOJ = dOJ;
	}	
	
}}